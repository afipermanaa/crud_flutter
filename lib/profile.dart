import 'dart:convert';

class Profile {
  int id;
  String name;
  String email;
  int age;

  Profile({this.id = 0, this.name, this.email, this.age});

  factory Profile.fromJson(Map<String, dynamic> map) { //ini merupakan named constructor buat paramater 4 field diatas
    return Profile(
        id: map["id"], name: map["name"], email: map["email"], age: map["age"]);
  }

  // factory berfungsi biar ga ngebuat object baru kalo kita manggil constructor diatas.

  Map<String, dynamic> toJson() { // ini buat mapping dari class model
    return {
      "id": id,
      "name": name,
      "email": email,
      "age": age,
    };
  }
  @override
String toString() {
  return 'Profile{id: $id, name: $name, email: $email, age: $age}';
}

}



List<Profile> profileFromJson(String jsonData) { // fungsi buat konversi respon dari api ke class model
  final data = json.decode(jsonData);
  return List<Profile>.from(data.map((item) => Profile.fromJson(item)));
}

String profileToJson(Profile data){ // ini buat ngubah dari json ke string
  final jsonData = data.toJson();
  return json.encode(jsonData);
}





